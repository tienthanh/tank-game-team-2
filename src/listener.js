const KeyBroadListener = cc.EventListener.create({
    event: cc.EventListener.KEYBOARD,
    swallowTouches: true,
    onKeyPressed: function (keyCode, event) {
        cc.audioEngine.playEffect(res.Snap_sound_effect_mp3);
        let target = event.getCurrentTarget();
        let position = target.getPosition();
        let size = target.getBoundingBox();
        switch (keyCode) {
            case KEY_CODE.LEFT:
                if (!(target.direction === DIRECTION.WEST && position.x - target.tankSpeed > size.width / 2)) {
                    target.direction = DIRECTION.WEST;
                    let turnLeft = cc.rotateTo(0.1, 90);
                    target.runAction(turnLeft);
                }
                target.speed = cc.p(-SPEED.TANK, 0);
                break;
            case KEY_CODE.RIGHT:
                if (!(target.direction === DIRECTION.EAST && position.x + target.tankSpeed < cc.winSize.width - size.width / 2)) {
                    target.direction = DIRECTION.EAST;
                    let turnRight = cc.rotateTo(0.1, -90);
                    target.runAction(turnRight);
                }
                target.speed = cc.p(SPEED.TANK, 0);
                break;
            case KEY_CODE.UP:
                if (!(target.direction === DIRECTION.NORTH && position.y + target.tankSpeed < cc.winSize.height - size.height / 2)) {
                    target.direction = DIRECTION.NORTH;
                    let moveForward = cc.rotateTo(0.1, 180);
                    target.runAction(moveForward);
                }
                target.speed = cc.p(0, SPEED.TANK);
                break;
            case KEY_CODE.DOWN:
                if (!(target.direction === DIRECTION.SOUTH && position.y - target.tankSpeed > size.height / 2)) {
                    target.direction = DIRECTION.SOUTH;
                    let moveBackward = cc.rotateTo(0.1, 0);
                    target.runAction(moveBackward);
                }
                target.speed = cc.p(0, -SPEED.TANK);
                break;
            case KEY_CODE.ENTER:
                target.shoot();
        }

        //target.setPosition(position);
    },
    onKeyReleased: function (keyCode, event) {
        let target = event.getCurrentTarget();
        target.speed = cc.p(0, 0);
    }
});
