
const EnemyType = [

    {
        type: 0,
        textureName: res.tank_bigRed_png,
        bulletType: res.bulletRed3_outline_png,
        HP: 1,
        moveType: ENEMY_MOVE_TYPE.ATTACK,
        scoreValue: 15
    },

    {
        type: 1,
        textureName: res.tank_huge_png,
        bulletType: res.bulletRed3_outline_png,
        HP: 3,
        moveType: ENEMY_MOVE_TYPE.ATTACK,
        scoreValue: 40
    }
];