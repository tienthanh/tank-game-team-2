const Enemy = cc.Sprite.extend({
    type: 1,
    speed: SPEED.ENEMY,
    direction: DIRECTION.SOUTH,
    HP: 15,
    active: true,
    bulletType: res.bulletRed3_outline_png,
    moveType: null,
    scoreValue: 200,
    ctor: function (arg) {
        this._super();
        this.initWithFile(arg.textureName);
        this.HP = arg.HP;
        this.moveType = arg.moveType;
        this.scoreValue = arg.scoreValue;
        this.type = arg.type;
        this.bulletType = arg.bulletType;
        this.schedule(this.shoot, 0.5);
        this.scheduleUpdate();
    },
    shoot: function () {
        let position = this.getPosition();
        const a = new Bullet(position.x + 10, position.y - this.getContentSize().height, this.bulletType, this.direction);
        const b = new Bullet(position.x - 10, position.y - this.getContentSize().height, this.bulletType, this.direction);
        gameLayer.addChild(a, 0);
        gameLayer.addChild(b, 0);
    },
    update: function () {
        if (this.active && this.HP <= 0 || this.getPosition().x > cc.winSize.x || this.getPosition().x < 0 || this.getPosition().y > cc.winSize.height || this.getPosition().y < 0) {
            this.initWithFile(res.explosion2_png);
            this.active = false;
            score += this.scoreValue;
            setTimeout(function () {
                let index = enemies.indexOf(this);
                if (index > -1) {
                    enemies.splice(index, 1);
                }
                gameLayer.removeChild(this);

            }.bind(this), 500);
        }
    }

});