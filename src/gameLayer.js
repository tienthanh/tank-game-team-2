const Game = cc.Layer.extend({
    tank: null,
    bulletVisible:false,
    init: function () {
        background = new cc.Sprite(res.background_png);
        background.attr({
            x: cc.winSize.width / 2,
            y: cc.winSize.height / 2,

        });
        this.addChild(background, -10);
        this.tank = new Tank(res.tank_blue_png);
        this.tank.rotation = 180;
        this.addChild(this.tank);
        this.addChild(this.tank.emitter);
        cc.eventManager.addListener(KeyBroadListener, this.tank);
        this.schedule(this.addEnemyToGameLayer, 10.0);
        this.addScoreText();
        this.addHP();
        this.scheduleUpdate();
        gameLayer=this;
    },
    addEnemyToGameLayer: function (enemyType) {
        const enemy = new Enemy(EnemyType[1]);
        this.addChild(enemy);
        enemies.push(enemy);
        enemy.x = 80 + (cc.winSize.width - 160) * Math.random();
        enemy.y = cc.winSize.height;
        let offset, tmpAction;

        switch (enemy.moveType) {
            case ENEMY_MOVE_TYPE.ATTACK:
                offset = cc.p(enemy.getPosition().x, cc.winSize.height / 2);
                tmpAction = cc.moveTo(4, offset);
                break;
            case ENEMY_MOVE_TYPE.VERTICAL:
                offset = cc.p(0, -cc.winSize.height - enemy.height);
                tmpAction = cc.moveBy(4, offset);
                break;

            case ENEMY_MOVE_TYPE.HORIZONTAL:
                offset = cc.p(0, -100 - 200 * Math.random());
                let a0 = cc.moveBy(0.5, offset);
                let a1 = cc.moveBy(1, cc.p(-50 - 100 * Math.random(), 0));
                const onComplete = cc.callFunc(function (pSender) {
                    const a2 = cc.delayTime(1);
                    const a3 = cc.moveBy(1, cc.p(100 + 100 * Math.random(), 0));
                    pSender.runAction(cc.sequence(a2, a3, a2.clone(), a3.reverse()).repeatForever());
                }.bind(enemy));
                tmpAction = cc.sequence(a0, a1, onComplete);
                break;
            case ENEMY_MOVE_TYPE.OVERLAP:
                let newX = (enemy.x <= cc.winSize.width / 2) ? cc.winSize.width : -cc.winSize.width;
                a0 = cc.moveBy(4, cc.p(newX, -cc.winSize.width * 0.75));
                a1 = cc.moveBy(4, cc.p(-newX, cc.winSize.width));
                tmpAction = cc.sequence(a0, a1);
                break;
        }
        enemy.runAction(tmpAction);
    },
    addScoreText: function () {
        score = 0;
        scoreText = cc.LabelTTF.create("Score: " + score,"Arial",32, null, cc.TEXT_ALIGNMENT_CENTER, cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(scoreText);
        scoreText.setPosition(scoreText.getBoundingBox().width, cc.winSize.height - scoreText.getBoundingBox().height);
    },
    addHP: function () {
        hp = cc.LabelTTF.create("HP: " + this.tank.HP,"Arial",32, null, cc.TEXT_ALIGNMENT_CENTER, cc.TEXT_ALIGNMENT_CENTER);
        this.addChild(hp);
        hp.setPosition(hp.getBoundingBox().width, cc.winSize.height - scoreText.getBoundingBox().height - hp.getBoundingBox().height);
    }
    ,update: function () {
        if(this.bulletVisible)
        scoreText.setString("Score: " + score);
        if (enemies.length === 0) {
            this.addEnemyToGameLayer(1);
        }
    }
});

