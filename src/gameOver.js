const gameOverScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        gameLayer = new gameOver();
        gameLayer.init();
        this.addChild(gameLayer);
    }
});

const gameOver = cc.Layer.extend({
    ctor:function(){
        this._super();
        this.init();
    },
    init: function () {
        let sp = new cc.Sprite(res.gameover_jpg);
        sp.scale= 0.5;
        this.setPosition(cc.winSize.width / 2, cc.winSize.height/2);
        this.addChild(sp, 0, 1);
    },
});