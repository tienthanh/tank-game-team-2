let gameLayer;
let background;
let scoreText;
let score;
let enemies = [];

const HP = {
    TANK: 50
};

const KEY_CODE = {
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    ENTER: 13
};

const SPEED = {
    TANK: 3,
    ENEMY: 8,
    BULLET: 8
};

const DIRECTION = {
    NORTH: 0,
    SOUTH: 1,
    EAST: 10,
    WEST: 11

};

const ENEMY_MOVE_TYPE = {
    ATTACK:0,
    VERTICAL:1,
    HORIZONTAL:2,
    OVERLAP:3
};
