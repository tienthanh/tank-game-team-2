const Tank = cc.Sprite.extend({
    tankSpeed: 0,
    speed: cc.p(0, 0),
    HP: HP.TANK,
    bulletType: res.bulletBlue3_outline_png,
    direction: DIRECTION.NORTH,
    emitter: null,
    ctor: function (img) {
        this._super();
        this.initWithFile(img);
        this.setPosition(cc.winSize.width / 2, cc.winSize.height / 2);
        this.emitter = cc.ParticleSun.create();
        let myTexture = cc.textureCache.addImage(res.particle_png);
        this.emitter.setTexture(myTexture);
        this.emitter.setStartSize(2);
        this.emitter.setEndSize(4);
        this.scheduleUpdate();
    },
    shoot: function () {
        cc.audioEngine.playEffect(res.Gunshot_sound_effect_wav);
        let offsetX = 0;
        let offsetY = 0;
        if (this.direction === DIRECTION.NORTH) {
            offsetX = 0;
            offsetY = 35;
        } else if (this.direction === DIRECTION.SOUTH) {
            offsetY = -35;
            offsetX = 0;
        } else if (this.direction === DIRECTION.WEST) {
            offsetX = -35;
            offsetY = 0;
        } else {
            offsetX = 35;
            offsetY = 0;
        }
        let bullet = new Bullet(this.getPosition().x + offsetX, this.getPosition().y + offsetY, this.bulletType, this.direction);
        gameLayer.addChild(bullet);

    },
    update: function () {

        // move tank
        if (!this.canMove()) this.speed = cc.p(0, 0);

        this.x += this.speed.x;
        this.y += this.speed.y;
        hp.setString("HP: " + this.HP);
        if (this.HP <= 0) {
            cc.director.runScene(new gameOverScene());
        }
        if (this.HP < HP.TANK / 3 * 2) {
            this.emitter.setVisible(true);
            this.emitter.setPosition(this.getPosition().x - 5, this.getPosition().y - 5);
        } else {
            //this.emitter.secollisiontVisible(false);
        }
    },
    canMove: function () {
        let x = this.x + this.speed.x - this.width / 2;
        let y = this.y + this.speed.y - this.height / 2;
        let rectTank = cc.rect(x, y, this.width, this.height);
        let rectTemp = null;
        let enemy = null;
        for (let i = 0; i < enemies.length; i++) {
            enemy = enemies[i];
            //rectTemp = cc.rect(enemy.x - enemy.width / 2, enemy.y - enemy.height / 2, enemy.width, enemy.height);
            rectTemp=enemy.getBoundingBox();
            if (cc.rectIntersectsRect(rectTank, rectTemp)) return false;
        }
        return true;
    }
});
