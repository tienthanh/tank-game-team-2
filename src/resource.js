var res = {
    HelloWorld_png : "res/HelloWorld.png",
    tank_blue_png: "res/tank_blue.png",
    bulletBlue3_outline_png: "res/bulletBlue3_outline.png",
    background_png: "res/background.png",
    tank_bigRed_png: "res/tank_bigRed.png",
    tank_huge_png: "res/tank_huge.png",
    bulletRed3_outline_png: "res/bulletRed3_outline.png",
    oilSpill_large_png: "res/oilSpill_large.png",
    explosion2_png: "res/explosion2.png",
    particle_png: "res/particle.png",
    gameover_jpg: "res/gameover.jpg",

    Gunshot_sound_effect_wav: "res/Gunshot_sound_effect.wav",
    Snap_sound_effect_mp3: "res/Snap_sound_effect.mp3"

};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
