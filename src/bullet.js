const Bullet = cc.Sprite.extend({
    bulletSpeed: SPEED.BULLET,
    xSpeed: 0,
    ySpeed: 0,
    direction: DIRECTION.NORTH,
    ctor: function (x, y, img, direction) {
        this._super();
        this.initWithFile(img);
        this.setPosition(x, y);
        this.direction = direction;
        if (this.direction === DIRECTION.NORTH) {
            this.xSpeed = 0;
            this.ySpeed = this.bulletSpeed;
        } else if (this.direction === DIRECTION.SOUTH) {
            this.xSpeed = 0;
            this.ySpeed = -this.bulletSpeed;
            this.rotation = 180;
        } else if (this.direction === DIRECTION.WEST) {
            this.xSpeed = -this.bulletSpeed;
            this.rotation = -90;
            this.ySpeed = 0;
        } else {
            this.xSpeed = this.bulletSpeed;
            this.rotation = 90;
            this.ySpeed = 0;
        }
        this.scheduleUpdate();
        return true;
    },
    update: function () {
        let bulletBoundingBox = this.getBoundingBox();
        if (cc.rectIntersectsRect(bulletBoundingBox, gameLayer.tank.getBoundingBox())) {
            gameLayer.tank.HP--;
            gameLayer.removeChild(this);
            return;
        }

        for (let i = 0; i < enemies.length; i++) {
            //cc.log(enemies[i].scoreValue);
            let enemyBoundingBox = enemies[i].getBoundingBox();
            if (cc.rectIntersectsRect(bulletBoundingBox, enemyBoundingBox)) {

                enemies[i].HP--;
                gameLayer.removeChild(this);

                return;
            }
        }
        this.setPosition(this.getPosition().x + this.xSpeed, this.getPosition().y + this.ySpeed);
        if (this.getPosition().x > cc.winSize.x || this.getPosition().x < 0 || this.getPosition().y > cc.winSize.height || this.getPosition().y < 0) {
            gameLayer.removeChild(this, true);
        }
    }
});